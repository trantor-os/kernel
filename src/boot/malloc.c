/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <stdint.h>
#include <mm.h>

/*
  A very simple and non-optimized memory allocator.

  Maintains a program break pointer. The heap area is above this. The
  heap is divided into a linked list of blocks which could be
  allocated or free.
*/

extern uint8_t boot_end;

static void* brk = &boot_end;

/* Each block has a header and footer */

typedef struct {
    // Size of block including header and footer
    uint32_t size;
} blk_footer_t;

typedef struct blk_header_t {
    blk_footer_t* footer;
    bool in_use;

    // This pointer is only valid when in_use == false
    struct blk_header_t *next;
} blk_header_t;

static blk_header_t *free_blks = 0;

static void add_heap(size_t sz)
{
    int npages = (sz+4095)/4096;
    blk_header_t *hdr = alloc_page(brk, npages);
    if (!hdr)
        return;

    // Merge with the last block if it is free
    blk_footer_t* pfooter = 0;
    blk_header_t* p = 0;
    if (brk > (void*)&boot_end) {
        pfooter = brk - sizeof(blk_footer_t);
        p = brk - pfooter->size;
    }

    brk += npages*4096;

    if (p == 0 || p->in_use) {
        hdr->footer = brk - sizeof(blk_footer_t);
        hdr->footer->size = npages*4096;
        hdr->in_use = false;
        hdr->next = free_blks;
        free_blks = hdr;
    } else {
        p->footer = brk - sizeof(blk_footer_t);
        p->footer->size = pfooter->size + npages*4096;
    }
}

void* malloc(size_t sz)
{
    const size_t header_size = sizeof(blk_header_t) + sizeof(blk_footer_t);
    sz += header_size;

    if (free_blks == 0)
        add_heap(sz);

    while (true) {
        for (blk_header_t *p = free_blks; p != 0; p = p->next) {
            if (p->footer->size < sz)
                continue;

            // split the block if needed
            if (p->footer->size > sz + header_size) {
                blk_header_t* n = (void*)p + sz;
                n->footer = p->footer;
                n->footer->size = p->footer->size - sz;
                n->in_use = false;
                n->next = p->next;

                p->footer = (void*)n - sizeof(blk_footer_t);
                p->footer->size = sz;
                p->next = n;
            }

            // Remove p from the free list
            p->in_use = true;
            if (free_blks == p)
                free_blks = p->next;

            return p + 1;
        }

        // We did not find any big enough block, add more heap
        add_heap(sz);
    }
}

void free(void* m)
{
    blk_header_t *hdr = m - sizeof(blk_header_t);

    if (hdr < (blk_header_t*)&boot_end || hdr >= (blk_header_t*)brk)
        return;

    bool merged = false;
    // Merge with next block if that is free
    blk_header_t *n = (blk_header_t*)(hdr->footer + 1);
    if ((void*)n < brk && !n->in_use) {
        hdr->footer = n->footer;
        hdr->in_use = false;
        hdr->footer->size = (void*)(n->footer + 1) - (void*)hdr;
        if (free_blks == n)
            free_blks = hdr;
        hdr->next = n->next;

        merged = true;
    }

    // Merge with previous block if that is free
    blk_header_t *p = 0;
    if (hdr > (blk_header_t*)&boot_end) {
        blk_footer_t* pfooter = (blk_footer_t*)hdr - 1;
        p = (void*)hdr - pfooter->size;
    }
    if (p && !p->in_use) {
        hdr->footer->size += p->footer->size;
        p->footer = hdr->footer;
        if ((void*)p->next < (void*)p->footer)
            p->next = hdr->next;

        merged = true;
    }

    if (!merged) {
        hdr->in_use = false;
        hdr->next = free_blks;
        free_blks = hdr;
    }
}
