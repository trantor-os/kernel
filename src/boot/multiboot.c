/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <multiboot.h>


struct multiboot_header mb_header ALIGN(4) SECTION(".multiboot") = {
    .magic     = 0xe85250d6,
    .arch      = 0,   /* x86 */
    .length    = sizeof(struct multiboot_header),
    .checksum  = 0 - 0xe85250d6 - sizeof(struct multiboot_header),

    .info_tag = {
        1,  // type = info request
        20, // size
        1,  // boot command line
        3,  // boot modules
        6   // memory map
    },

    .console_tag = {
        4,  // type = console flags
        12, // size
        3   // need a console and supports EGA text mode
    },

    .align_tag = {
        6,  // type = page aligned modules
        8   // size
    },

    .end_tag = {
        0,  // type = end of tags
        8   // size
    }
};

char mb_cmdline[128];
multiboot_module mb_mods[4];
multiboot_mmap mb_mmap[32];
