/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


#include <asm.h>
#include <pmode.h>
#include <irq.h>
#include <multiboot.h>
#include <interrupt.h>
#include <mm.h>

static void remap_pic()
{
    // initialize both PICs with icw4
    outb(0x20, 0x11);
    outb(0xa0, 0x11);

    // set IRQ base vector
    outb(0x21, 0x78);
    outb(0xa1, 0x70);

    // set up cascading
    outb(0x21, 0x04);
    outb(0xa1, 0x02);

    // enable x86 mode
    outb(0x21, 0x01);
    outb(0xa1, 0x01);
}

static void init_idt()
{
    void set_idt_entry(int vector, void* isr)
    {
        idt_entry_t* e = &idt[vector];
        e->offset_low = (uint32_t)isr & 0xffff;
        e->offset_high = (uint32_t)isr >> 16;
        e->seg_sel = KERNEL_CS;
        e->type = 0x0e;
        e->desc_type = 0;
        e->dpl = 0;
        e->present = 1;
    }

    // Exceptions
    set_idt_entry(0x00, isr_0x00);
    set_idt_entry(0x01, isr_0x01);
    set_idt_entry(0x02, isr_0x02);
    set_idt_entry(0x03, isr_0x03);
    set_idt_entry(0x04, isr_0x04);
    set_idt_entry(0x05, isr_0x05);
    set_idt_entry(0x06, isr_0x06);
    set_idt_entry(0x07, isr_0x07);
    set_idt_entry(0x08, isr_0x08);
    set_idt_entry(0x0a, isr_0x0a);
    set_idt_entry(0x0b, isr_0x0b);
    set_idt_entry(0x0c, isr_0x0c);
    set_idt_entry(0x0d, isr_0x0d);
    set_idt_entry(0x0e, isr_0x0e);
    set_idt_entry(0x10, isr_0x10);

    // IRQs
    set_idt_entry(0x70, isr_0x70);
    set_idt_entry(0x71, isr_0x71);
    set_idt_entry(0x72, isr_0x72);
    set_idt_entry(0x73, isr_0x73);
    set_idt_entry(0x74, isr_0x74);
    set_idt_entry(0x75, isr_0x75);
    set_idt_entry(0x76, isr_0x76);
    set_idt_entry(0x77, isr_0x77);
    set_idt_entry(0x78, isr_0x78);
    set_idt_entry(0x79, isr_0x79);
    set_idt_entry(0x7a, isr_0x7a);
    set_idt_entry(0x7b, isr_0x7b);
    set_idt_entry(0x7c, isr_0x7c);
    set_idt_entry(0x7d, isr_0x7d);
    set_idt_entry(0x7e, isr_0x7e);
    set_idt_entry(0x7f, isr_0x7f);

    // OS/BIOS interfaces
    //set_idt_entry(0x21, dos_21_isr);

    struct {
        uint16_t limit;
        uint32_t base;
    } PACKED idt_desc = { sizeof(idt) - 1, (uint32_t)idt };

    __asm__ volatile("lidt %0" : : "m"(idt_desc));
}

extern uint8_t boot_start, boot_end, kernel_start, kernel_end;

static void init_mm()
{
    extern uint8_t mm_bitmap[];
    extern unsigned mm_bitmap_max_idx;

    mm_bitmap_max_idx = 0;
    for (int i = 0; mb_mmap[i].base != 0 || mb_mmap[i].length != 0; i++) {
        multiboot_mmap e = mb_mmap[i];
        if (e.type != 1 || e.base > 0xffffffff)
            continue;

        // page align base and length
        unsigned start = ((uint32_t)e.base + 4095) / 4096;
        unsigned count = ((uint32_t)e.length + 4095) / 4096;
        for (unsigned idx = start; idx < start+count; idx++) {
            // skip IVT area and memory > 4GB
            if (idx == 0 || idx > 0xfffff)
                continue;
            mm_bitmap[idx / 8] |= 1 << (idx % 8);
            if (idx > mm_bitmap_max_idx)
                mm_bitmap_max_idx = idx;
        }
    }

    // Mark kernel and boot areas as used
    int start = (uint32_t)&boot_start / 4096;
    int end = (uint32_t)&kernel_end - (uint32_t)&kernel_start + (uint32_t)&boot_end;
    for (int idx = start; idx < end / 4096; idx++) {
        mm_bitmap[idx / 8] &= ~(1 << (idx % 8));
    }

    // Mark unused portions of mm_bitmap as available
    int last_used_addr = (uint32_t)&mm_bitmap[mm_bitmap_max_idx/8] - (uint32_t)&kernel_start + (uint32_t)&boot_end;
    int end_of_bitmap = (uint32_t)&mm_bitmap[128*1024] - (uint32_t)&kernel_start + (uint32_t)&boot_end;
    for (int idx = last_used_addr/4096 + 1; idx < end_of_bitmap/4096; idx++) {
        mm_bitmap[idx / 8] |= 1 << (idx % 8);
    }
}

static void init_tss()
{
    extern uint8_t kernel_stack;
    alloc_page(&kernel_stack, 1);

    tss.esp0 = (uint32_t)&kernel_stack + 4096;
    tss.ss0 = KERNEL_DS;
    tss.cr3 = (uint32_t)page_dir;
    tss.io_map_base = (void*)&tss.io_perms[0] - (void*)&tss;

    gdt_entry_t* e = &gdt[TSS_SELECTOR >> 3];
    e->limit_low = sizeof(tss_t) & 0xffff;
    e->limit_high = sizeof(tss_t) >> 16;
    e->base_low = (uint32_t)&tss & 0xffff;
    e->base_mid = ((uint32_t)&tss >> 16) & 0xff;
    e->base_high = ((uint32_t)&tss >> 24) & 0xff;
    e->type = 0x9;
    e->desc_type = 0;
    e->dpl = 0;
    e->present = 1;
    e->op_size = 0;
    e->granularity = 0;

    uint16_t sel = TSS_SELECTOR;
    __asm__ volatile("ltrw %0" : : "r"(sel));
}

static void init_drivers ()
{
    extern void init_timer();

    init_timer();
}

void init()
{
    remap_pic();
    init_mm();
    init_tss();
    init_idt();
    init_drivers();
}
