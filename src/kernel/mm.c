/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <stdint.h>
#include <attribs.h>
#include <asm.h>
#include <mm.h>

uint32_t kernel_page_tbl[1024] ALIGN(4096);

/*
  The top 128MB is reserved for kernel:
    0xffc00000 - 0xffffffff: Page directory and page tables
    0xff800000 - 0xffbfffff: Kernel code, data, bss, stack
    0xf8000000 - 0xfc000000: Kernel heap

  The rest is reserved for user space.
*/

/*
  The physical memory can be upto 4GB. The bitmap is located in the
  kernel and has a max size of 128KB. This max size is allocated here
  but the unused portion of it will be released during initialization.
  A set bit indicates that the page is available for allocation.
*/
uint32_t mm_bitmap[32768] ALIGN(4096);

/*
  The last bit index in mm_bitmap.
*/
unsigned mm_bitmap_max_idx = 0;

static volatile uint32_t* const page_dir = (uint32_t*)0xfffff000;
static volatile uint32_t* const page_tbls = (uint32_t*)0xffc00000;

/*
  Allocate a physical page and return its address.

  Returns 0 if no more physical memory can be allocated.
 */
static uint32_t alloc_phys_page()
{
    for (int i = mm_bitmap_max_idx/32; i >= 0; i--) {
        if (mm_bitmap[i] != 0) {
            uint32_t p = highest_set_bit(mm_bitmap[i]);
            mm_bitmap[i] &= ~(1 << p);
            return ((uint32_t)i * 32 + p) << 12;
        }
    }
    return 0;
}

/*
  Free an allocated page.
 */
static void free_phys_page(uint32_t p)
{
    p >>= 12;
    if (p == 0)
        return;

    mm_bitmap[p / 32] |= 1 << (p % 32);
}

static bool alloc_one_page(void* addr)
{
    uint32_t attrs = addr >= (void*)SUPERVISOR_SPACE_START ? 3 : 7;
    uint32_t dir = (uint32_t)addr >> 22;
    uint32_t tbl = (uint32_t)addr >> 12;

    if ((page_dir[dir] & 1) == 0) {
        uint32_t p = alloc_phys_page();
        if (p == 0)
            return false;
        page_dir[dir] = (p & 0xfffff000) + attrs;

        // zero the page table
        for (int i = dir << 10; i < (dir+1) << 10; i++)
            page_tbls[i] = 0;
    }

    if (page_tbls[tbl] != 0)
        return false;

    uint32_t p = alloc_phys_page();
    if (p == 0)
        return 0;

    page_tbls[tbl] = (p & 0xfffff000) + attrs;

    // zero the page
    uint32_t* ptr = addr;
    for (int i = 0; i < 1024; i++)
        *ptr++ = 0;

    return true;
}

/*
  Allocate memory for `count` pages at the specified linear address.

  Returns a pointer to start of page if allocation succeeded. Returns
  0 if allocation failed.  The allocation can fail if the linear
  address is already allocated or physical memory is not available.

  Maximum allowed value for count is 4096 (16MB).
 */
void* alloc_page(void* addr, int count)
{
    if (count > 4096)
        return 0;

    addr = (void*)((uint32_t)addr & 0xfffff000);

    for (int i = 0; i < count; i++) {
        if (!alloc_one_page(addr + i*4096)) {
            for (int j = i - 1; j >= 0; j--)
                free_page(addr + j*4096);
            return 0;
        }
    }

    return addr;
}

/*
  Free an allocated page.

  This is a no-op is there is no allocated page at the specified
  address.
 */
void free_page(void* addr)
{
   extern uint8_t kernel_start;

   uint32_t tbl = (uint32_t)addr >> 12;

   // You can't free kernel & paging structures
   if (addr >= (void*)&kernel_start)
       return;

   free_phys_page(page_tbls[tbl] & 0xfffff000);

   page_tbls[tbl] = 0;
   invlpg(addr);

   // Check if the entire page table can be freed
   int start = (tbl/1024)*1024;
   for (int idx = start; idx < start+1024; idx++) {
       if (page_tbls[idx] != 0)
           return;
   }

   uint32_t dir = (uint32_t)addr >> 22;
   free_phys_page(page_dir[dir] & 0xfffff000);
   page_dir[dir] = 0;
}
