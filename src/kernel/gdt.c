/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <stdint.h>
#include <attribs.h>
#include <pmode.h>

tss_t tss ALIGN(4096);

#define GDT_LENGTH 6

gdt_entry_t gdt[GDT_LENGTH] ALIGN(8) = {
    // null descriptor
    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
    // code segment, dpl = 0
    {
        .limit_low   = 0xffff,
        .base_low    = 0,
        .base_mid    = 0,
        .type        = 0b1010,
        .desc_type   = 1,
        .dpl         = 0,
        .present     = 1,
        .limit_high  = 0xf,
        .unused      = 0,
        .op_size     = 1,
        .granularity = 1,
        .base_high   = 0
    },
    // data segment, dpl = 0
    {
        .limit_low   = 0xffff,
        .base_low    = 0,
        .base_mid    = 0,
        .type        = 0b0010,
        .desc_type   = 1,
        .dpl         = 0,
        .present     = 1,
        .limit_high  = 0xf,
        .unused      = 0,
        .op_size     = 1,
        .granularity = 1,
        .base_high   = 0
    },
    // code segment, dpl = 3
    {
        .limit_low   = 0xffff,
        .base_low    = 0,
        .base_mid    = 0,
        .type        = 0b1010,
        .desc_type   = 1,
        .dpl         = 3,
        .present     = 1,
        .limit_high  = 0xf,
        .unused      = 0,
        .op_size     = 1,
        .granularity = 1,
        .base_high   = 0
    },
    // data segment, dpl = 3
    {
        .limit_low   = 0xffff,
        .base_low    = 0,
        .base_mid    = 0,
        .type        = 0b0010,
        .desc_type   = 1,
        .dpl         = 3,
        .present     = 1,
        .limit_high  = 0xf,
        .unused      = 0,
        .op_size     = 1,
        .granularity = 1,
        .base_high   = 0
    },
    // TSS (filled in load_tss)
    {
        .limit_low   = 0,
        .base_low    = 0,
        .base_mid    = 0,
        .type        = 0b1001,
        .desc_type   = 0,
        .dpl         = 0,
        .present     = 1,
        .limit_high  = 0,
        .unused      = 0,
        .op_size     = 0,
        .granularity = 0,
        .base_high   = 0
    },
};

typedef struct {
    uint16_t limit;
    uint32_t base;
} PACKED gdt_desc_t;

gdt_desc_t gdt_desc = {
    .limit = sizeof(gdt) - 1,
    .base  = (uint32_t)gdt
};
