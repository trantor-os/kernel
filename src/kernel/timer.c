/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <attribs.h>
#include <pmode.h>
#include <irq.h>

typedef struct {
    uint16_t year;
    uint8_t
        month,     // 1-12
        day,       // 1-31
        hour,      // 0-23
        minute,    // 0-59
        second,    // 0-59
        centisec;  // 0-100
} time_t;

time_t now;

static uint8_t read_cmos_reg(uint8_t reg)
{
    outb(0x70, reg);
    return inb(0x71);
}

static void write_cmos_reg(uint8_t reg, uint8_t val)
{
    outb(0x70, reg);
    outb(0x71, val);
}

static void refresh_clock()
{
    bool binary = false, clock24 = false;
    time_t new_time;

    uint8_t v = read_cmos_reg(0x0b);
    binary = (v & 0x04) != 0;
    clock24 = (v & 0x02) != 0;

    uint8_t read_rtc_reg(uint8_t reg)
    {
        uint8_t v = read_cmos_reg(reg);
        return binary ? v : (((v >> 4) * 10) + (v & 0xf));
    }

    new_time.year = read_rtc_reg(0x09);
    if (new_time.year >= 80)
        new_time.year += 1900;
    else
        new_time.year += 2000;
    new_time.month = read_rtc_reg(0x08);
    new_time.day = read_rtc_reg(0x07);
    new_time.hour = read_rtc_reg(0x04);
    if (!clock24) {
        new_time.hour--;
        if (new_time.hour & 0x80)
            new_time.hour += 12;
    }
    new_time.minute = read_rtc_reg(0x02);
    new_time.second = read_rtc_reg(0x00);
    new_time.centisec = 0;

    if (now.year == new_time.year &&
        now.month == new_time.month &&
        now.day == new_time.day &&
        now.hour == new_time.hour &&
        now.minute == new_time.minute &&
        now.second == new_time.second)
        now.centisec = (now.centisec + 2) % 100;
    else {
        now = new_time;

        uint8_t* p = (uint8_t*)0xb8004;
        (*p)++;
    }
}

void rtc_handler(UNUSED intr_frame_t* frame)
{
    static volatile unsigned counter = 0;

    if (++counter == 25) {
        counter = 0;
        refresh_clock();
    }

    // This is required to enable the IRQ again
    read_cmos_reg(0x0c);

    send_eoi_high();
}

void init_timer()
{
    // read register B, and disable NMI
    uint8_t prev = read_cmos_reg(0x8b);

    // set bit 6 in register B to enable IRQs
    write_cmos_reg(0x8b, prev | 0x40);

    // Enable NMI and read register C to enable next interrupt
    read_cmos_reg(0x0c);
}
