/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <mm.h>
#include <stdint.h>

/*
  Kernel heap grows upward from 0xf8000000. It can't exceed 0xfc000000
  (64 MB).
*/

/* Pointer to the end of kernel heap */
static void* kbrk = (void*)SUPERVISOR_SPACE_START;

/*
  Memory blocks are allocated in fixed sized bins. Each bin can have
  lengths between 32 bytes and 16MB including the block header.
*/
#define MIN_EXP 5
#define MAX_EXP 24

typedef struct block_header_t {
    uint32_t magic;
    uint32_t index;
    struct block_header_t *prev, *next;
} block_header_t;

#define HEADER_MAGIC 0x424c4b48    // 'BLKH'

typedef struct {
    block_header_t *free_blocks, *used_blocks;
} bin_t;

static bin_t bins[MAX_EXP - MIN_EXP + 1] = {
    [0 ... (MAX_EXP - MIN_EXP)] = { 0, 0 }
};

static int index_of(size_t sz)
{
    for (int i = MIN_EXP; i <= MAX_EXP; i++) {
        if (sz <= 1 << i)
            return i - MIN_EXP;
    }
    return -1;
}

void* kmalloc(size_t sz)
{
    if (sz == 0)
        return 0;

    int idx = index_of(sz + sizeof(block_header_t));
    if (idx < 0 || idx > MAX_EXP - MIN_EXP)
        return 0;

    // Round up the size
    sz = 1 << (idx + MIN_EXP);

    if (bins[idx].free_blocks == 0) {
        int page_count = (sz + 4095) / 4096;
        void* p = alloc_page(kbrk, page_count);
        if (p == 0)
            return 0;

        kbrk += page_count*4096;
        bins[idx].free_blocks = p;

        // Divide pages to blocks
        block_header_t *ptr = p, *prev = 0;
        while ((void*)ptr < kbrk) {
            ptr->magic = HEADER_MAGIC;
            ptr->index = idx;
            ptr->prev = prev;
            prev = ptr;
            ptr = (void*)ptr + sz;
            prev->next = ptr;
        }
        prev->next = 0;
    }

    // Remove a block from free list
    block_header_t* blk = bins[idx].free_blocks;
    block_header_t* nnext = blk->next;
    if (nnext != 0)
        nnext->prev = 0;
    bins[idx].free_blocks = nnext;

    // Add the block to the head of used list
    block_header_t* used = bins[idx].used_blocks;
    blk->next = used;
    if (used != 0)
        used->prev = blk;
    bins[idx].used_blocks = blk;

    return (void*)blk + sizeof(block_header_t);
}

extern void kfree(void* m)
{
    block_header_t* p = m - sizeof(block_header_t);
    if (p->magic != HEADER_MAGIC || p->index > MAX_EXP - MIN_EXP)
        return;

    // Make sure that p is part of a bin
    for (block_header_t* ptr = bins[p->index].used_blocks; ptr != 0; ptr = ptr->next) {
        if (ptr != p)
            continue;

        // Remove it from used_blocks
        if (p->prev)
            p->prev->next = p->next;
        else
            bins[p->index].used_blocks = p->next;
        if (p->next)
            p->next->prev = p->prev;

        // If p->next is 0, one or more pages can be freed here. Its
        // not done because its not expected that not much memory is
        // expected to be freed in kernel heap.

        // Add it to free_blocks
        p->prev = 0;
        p->next = bins[p->index].free_blocks;
        if (p->next)
            p->next->prev = p;
        bins[p->index].free_blocks = p;
    }
}
