/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <pmode.h>


        .text

        .macro define_isr int_no, isr_func

        .align  4
        .global isr_\int_no
isr_\int_no:
        pushal
        cld

        movw    $KERNEL_DS, %ax
        movw    %ax, %ds
        movw    %ax, %es

        call    \isr_func

        popal
        iret

        .endm


        define_isr      0x00, divide_error
        define_isr      0x01, debug_exception
        define_isr      0x02, nmi_interrupt
        define_isr      0x03, breakpoint_exception
        define_isr      0x04, overflow_exception
        define_isr      0x05, bound_exception
        define_isr      0x06, invalid_opcode
        define_isr      0x07, device_not_available
        define_isr      0x08, double_fault
        define_isr      0x0a, invalid_tss
        define_isr      0x0b, segment_not_present
        define_isr      0x0c, stack_fault
        define_isr      0x0d, general_protection_fault
        define_isr      0x0e, page_fault
        define_isr      0x10, fp_error

        // IRQs
        define_isr      0x70, rtc_handler
        define_isr      0x71, high_irq
        define_isr      0x72, high_irq
        define_isr      0x73, high_irq
        define_isr      0x74, high_irq
        define_isr      0x75, high_irq
        define_isr      0x76, high_irq
        define_isr      0x77, high_irq
        define_isr      0x78, low_irq
        define_isr      0x79, low_irq
        define_isr      0x7a, low_irq
        define_isr      0x7b, low_irq
        define_isr      0x7c, low_irq
        define_isr      0x7d, low_irq
        define_isr      0x7e, low_irq
        define_isr      0x7f, low_irq

high_irq:
        movb    $0x20, %al
        outb    %al, $0xa0
        outb    %al, $0x20
        ret

low_irq:
        movb    $0x20, %al
        outb    %al, $0x20
        ret
