/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <attribs.h>
#include <stdint.h>
#include <pmode.h>

void divide_error(intr_frame_t* frame) {}
void debug_exception(intr_frame_t* frame) {}
void nmi_interrupt(intr_frame_t* frame) {}
void breakpoint_exception(intr_frame_t* frame) {}
void overflow_exception(intr_frame_t* frame) {}
void bound_exception(intr_frame_t* frame) {}
void invalid_opcode(intr_frame_t* frame) {}
void device_not_available(intr_frame_t* frame) {}
void double_fault(intr_frame_t* frame) {}
void invalid_tss(intr_frame_t* frame) {}
void segment_not_present(intr_frame_t* frame) {}
void stack_fault(intr_frame_t* frame) {}
void general_protection_fault(intr_frame_t* frame) {}
void page_fault(intr_frame_t* frame) {}
void fp_error(intr_frame_t* frame) {}

idt_entry_t idt[256] ALIGN(8) = {
    [0 ... 255] = {
        .offset_low  = 0,
        .seg_sel     = 0,
        .unused      = 0,
        .type        = 0,
        .desc_type   = 0,
        .dpl         = 0,
        .present     = 0,
        .offset_high = 0
    }
};
