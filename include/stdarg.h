/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef STDARG_H
#define STDARG_H

typedef char* va_list;

/* Guarentees that type takes at least sizeof(void*) bytes in the stack */
#define _va_round(type) ((sizeof(type) + sizeof(void*) - 1) & ~(sizeof(void*) - 1))

#define va_start(ap, paramN) ((ap) = (char*)&paramN + (_va_round(paramN)))
#define va_end(ap) ((ap) = (void*)0)
#define va_arg(ap, type) ((ap) += (_va_round(type)), (*(type*) ((ap) - (_va_round(type)))))

#endif
