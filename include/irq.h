/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef IRQ_H
#define IRQ_H

#include <attribs.h>
#include <pmode.h>
#include <asm.h>

static inline void send_eoi_low()
{
    outb(0x20, 0x20);
}

static inline void send_eoi_high()
{
    outb(0xa0, 0x20);
    outb(0x20, 0x20);
}

#endif
