/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


#ifndef MM_H
#define MM_H

#include <stdint.h>

#define SUPERVISOR_SPACE_START 0xf8000000

extern void* alloc_page(void* addr, int count);
extern void free_page(void* addr);

extern void* kmalloc(size_t size);
extern void kfree(void* m);

extern void* malloc(size_t size);
extern void free(void* m);

#endif
