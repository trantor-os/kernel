/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


#ifndef PMODE_H
#define PMODE_H

#define KERNEL_CS      0x08
#define KERNEL_DS      0x10
#define USER_CS        0x1b
#define USER_DS        0x23
#define TSS_SELECTOR   0x28

#ifndef __ASSEMBLER__

#include <attribs.h>
#include <stdint.h>

typedef struct {
    uint32_t edi;
    uint32_t esi;
    uint32_t ebp;
    uint32_t unused;
    uint32_t ebx;
    uint32_t edx;
    uint32_t ecx;
    uint32_t eax;
    uint32_t error_code;
    uint32_t eip;
    uint32_t cs;
    uint32_t eflags;
    uint32_t esp;
    uint32_t ss;
    uint32_t es;
    uint32_t ds;
    uint32_t fs;
    uint32_t gs;
} PACKED intr_frame_t;

typedef struct {
    uint16_t offset_low;
    uint16_t seg_sel;
    uint8_t  unused;
    unsigned type : 4;
    unsigned desc_type : 1;
    unsigned dpl : 2;
    unsigned present : 1;
    uint16_t offset_high;
} PACKED idt_entry_t;

extern idt_entry_t idt[256];

typedef struct {
    uint16_t prev_task;
    uint16_t reserved_0;
    uint32_t esp0;
    uint16_t ss0;
    uint16_t reserved_1;
    uint32_t esp1;
    uint16_t ss1;
    uint16_t reserved_2;
    uint32_t esp2;
    uint16_t ss2;
    uint16_t reserved_3;
    uint32_t cr3;
    uint32_t eip;
    uint32_t eflags;
    uint32_t eax, ecx, edx, ebx, esp, ebp, esi, edi;
    uint16_t es;
    uint16_t reserved_4;
    uint16_t cs;
    uint16_t reserved_5;
    uint16_t ss;
    uint16_t reserved_6;
    uint16_t ds;
    uint16_t reserved_7;
    uint16_t fs;
    uint16_t reserved_8;
    uint16_t gs;
    uint16_t reserved_9;
    uint16_t ldr_selector;
    uint32_t reserved_10;
    uint16_t io_map_base;
    uint8_t  interrupt_redirs[32];
    uint8_t  io_perms[8193];
} PACKED tss_t;

extern tss_t tss;

extern uint32_t page_dir[];

typedef struct {
    uint16_t limit_low;
    uint16_t base_low;
    uint8_t  base_mid;
    unsigned type : 4;
    unsigned desc_type : 1;
    unsigned dpl : 2;
    unsigned present : 1;
    unsigned limit_high : 4;
    unsigned unused : 2;
    unsigned op_size : 1;
    unsigned granularity : 1;
    uint8_t  base_high;
} PACKED gdt_entry_t;

extern gdt_entry_t gdt[];

#endif

#endif
