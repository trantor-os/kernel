/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


#ifndef INTERRUPT_H
#define INTERRUPT_H

#include <pmode.h>

extern void isr_0x00(), isr_0x01(), isr_0x02(), isr_0x03(),
    isr_0x04(), isr_0x05(), isr_0x06(), isr_0x07(),
    isr_0x08(), isr_0x0a(), isr_0x0b(), isr_0x0c(),
    isr_0x0d(), isr_0x0e(), isr_0x10();

extern void isr_0x70(), isr_0x71(), isr_0x72(), isr_0x73(),
    isr_0x74(), isr_0x75(), isr_0x76(), isr_0x77(),
    isr_0x78(), isr_0x79(), isr_0x7a(), isr_0x7b(),
    isr_0x7c(), isr_0x7d(), isr_0x7e(), isr_0x7f();

#endif
