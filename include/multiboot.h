/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef MULTIBOOT_H
#define MULTIBOOT_H

#include <stdint.h>
#include <attribs.h>

struct multiboot_header {
    uint32_t magic, arch, length, checksum;

    uint32_t info_tag[5] ALIGN(8);     /* info request tags */
    uint32_t console_tag[3] ALIGN(8);  /* console tag */
    uint32_t align_tag[2] ALIGN(8);    /* module alignment tag*/
    uint32_t end_tag[2] ALIGN(8);
} PACKED;

typedef struct {
    uint32_t start;
    uint32_t end;
    char cmdline[128];
} PACKED multiboot_module;

typedef struct {
    uint64_t base;
    uint64_t length;
    uint32_t type;
} PACKED multiboot_mmap;

extern char mb_cmdline[];
extern multiboot_module mb_mods[];
extern multiboot_mmap mb_mmap[];

#endif
