/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


#ifndef ASM_H
#define ASM_H

#include <stdint.h>

static inline void sti()
{
    __asm__ volatile("sti");
}

static inline void cli()
{
    __asm__ volatile("cli");
}

static inline uint8_t inb(uint16_t port)
{
    uint8_t ret;
    __asm__ volatile("inb %1, %0" : "=a"(ret) : "N"(port));
    return ret;
}

static inline void outb(uint16_t port, uint8_t val)
{
    __asm__ volatile("outb %0, %1" : : "a"(val), "N"(port));
}

static inline void invlpg(void* m)
{
    __asm__ volatile("invlpg %0" : : "m"(m) : "memory");
}

static inline uint32_t highest_set_bit(uint32_t v)
{
    uint32_t ret;
    __asm__ volatile("bsrl %1, %0" : "=r"(ret) : "g"(v));
    return ret;
}

static inline uint32_t lowest_set_bit(uint32_t v)
{
    uint32_t ret;
    __asm__ volatile("bsfl %1, %0" : "=r"(ret) : "g"(v));
    return ret;
}

#endif
