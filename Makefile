#
# Copyright (C) Raghu Kaippully. All rights reserved.
#

TOOLS_PATH  := $(abspath ../shilpi/tools/bin)
TARGET      := i586-elf
INCLUDE_DIR := $(abspath ./include)

CC := $(TOOLS_PATH)/$(TARGET)-gcc
AS := $(TOOLS_PATH)/$(TARGET)-gcc
LD := $(TOOLS_PATH)/$(TARGET)-ld

CFLAGS  := -Wall -O3 -march=pentium-mmx -m32 -isystem $(INCLUDE_DIR) -nostdinc -fno-builtin
ASFLAGS := -Wall -isystem $(INCLUDE_DIR)
LDFLAGS := -nostdlib -nostartfiles -nodefaultlibs


BUILD_DIR := build

$(BUILD_DIR)/%.o: src/%.s
	@echo "Assembling" $^
	@mkdir -p $(dir $@)
	@$(AS) $(ASFLAGS) -c -o $@ $^

$(BUILD_DIR)/%.o: src/%.S
	@echo "Assembling" $^
	@mkdir -p $(dir $@)
	@$(AS) $(ASFLAGS) -c -o $@ $^

$(BUILD_DIR)/%.o: src/%.c
	@echo "Compiling" $^
	@mkdir -p $(dir $@)
	@$(CC) $(CFLAGS) -c -o $@ $^

OBJS := $(addprefix $(BUILD_DIR)/,          \
             boot/start.o                   \
             boot/multiboot.o               \
             boot/main.o                    \
             boot/malloc.o                  \
             kernel/kdebug.o                \
             kernel/gdt.o                   \
	     kernel/isr.o                   \
             kernel/interrupt.o             \
             kernel/timer.o                 \
             kernel/kmalloc.o               \
             kernel/mm.o                    \
         )

$(BUILD_DIR)/kernel.sys: $(OBJS) src/kernel.ld
	@echo "Linking" $@
	@$(LD) $(LDFLAGS) -T src/kernel.ld -Map $(BUILD_DIR)/kernel.map -o $@ $(OBJS)


clean:
	rm -rf build
